const Product = require("../models/Product")
//const Course = require("../models/Course")

module.exports.viewAllProducts = () =>{
	return Product.find()
	.then(result=>{
		return result
	})
}

module.exports.addProduct = (reqBody,reqFile)=>{
	//creates variable "newUser" ans instantiates a new "User" object using the mongoose model
	//console.log(upload)

	let newProduct = new Product({
		productName: reqBody.productName,
		productDescription: reqBody.productDescription,
		price: reqBody.price,
		stock: reqBody.stock,
		category: reqBody.category,
		image: reqFile.filename
	})
	
	//save the created object to our database
	return  newProduct.save()
	.then((product, error)=>{
		if(error){
			return false
		}
		else{

			console.log(product)

			return true

		}
	})
}