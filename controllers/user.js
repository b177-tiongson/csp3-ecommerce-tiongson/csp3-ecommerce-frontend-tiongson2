const User = require("../models/User")
//const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//controller function for checking email duplicates
module.exports.checkEmailExist = (reqBody)=>{
	return User.find({email: reqBody.email})
	.then(result => {
		if(result.length > 0){
			//with duplicate
			return true
		}
		else{
			//without duplicate
			return false
		}
	})

}

//controller function for user registration

module.exports.registerUser = (reqBody)=>{
	//creates variable "newUser" ans instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		address: reqBody.address,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//save the created object to our database
	return newUser.save()
	.then((user, error)=>{
		if(error){
			return false
		}
		else{
			return true

		}
	})
}

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email})
	.then(result=>{
		//if user does no exist
		if(result.length > 0){

			return false
		}
		//user exist
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				//generate an access token
				return{access: auth.createAccessToken(result)}
			}
			//passwords do not match
			else{
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

}

module.exports.viewAllUsers = () =>{
	return User.find()
	.then(result=>{
		return result
	})
}