const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

//Allows access to routes defined within the application
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")

const app = express()

//connect to our mongoDB database
mongoose.connect("mongodb+srv://admin:CELxOfPENWdbDF3P@wdc028-course-booking.dhurq.mongodb.net/csp3-ecommerce?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
})

//prompts a message for successfull database connection
mongoose.connection.once('open',()=>console.log('Now connected to MongoDB Atlas'))

//allow all resources to access the backend application
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/uploads',express.static('uploads'))

//defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes)

//defines the "/product" string to be included for all course routes in the "course" route file
app.use("/products", productRoutes)

app.listen(process.env.PORT||4000,()=>{
	console.log(`API is now online on port ${process.env.PORT|| 4000}`)
})