const express = require("express")
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")

router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExist(req.body)
	.then(resultFromController => res.send(resultFromController))

})

router.post("/register",(req,res)=>{
	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController))
})

router.post("/login",(req,res)=>{
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

})

router.get("/all",(req,res)=>{

	userController.viewAllUsers()
	.then(resultFromController => res.send(resultFromController))

})

module.exports = router