const express = require("express")
const router = express.Router()
const productController = require("../controllers/product")
const upload = require('../middleware/upload')


router.get("/all",(req,res)=>{

	productController.viewAllProducts()
	.then(resultFromController => res.send(resultFromController))

})

router.post("/addProduct",upload.single('image'),(req,res)=>{


	productController.addProduct(req.body,req.file)
	.then(resultFromController => res.send(resultFromController))
})




module.exports = router