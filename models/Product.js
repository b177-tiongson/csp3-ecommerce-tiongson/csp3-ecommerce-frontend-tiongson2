const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({

	productName:{
		type:String,
		required:[true, "Product Name required"]
	},
	productDescription:{
		type:String,
		required:[true, "Product Description required"]

	},
	price:{
		type:Number,
		required:[true, "Price required"]

	},
	stock:{
		type:Number,
		required:[true, "Stock required"]

	},
	category:{
		type:String,
		required:[true, "Category required"]

	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	isActive:{
		type: Boolean,
		default: true
	},
	image:{
		type:String,
		//required:[true, "image required"]
		// data:Buffer,
		// contentType:String
	}

})


module.exports = mongoose.model("Product", productSchema)